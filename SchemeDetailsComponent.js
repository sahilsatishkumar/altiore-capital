import React from 'react'
import {
  Button,
  Text,
  ListItem,
  Header,
  Card,
  Left,
  Right,
  Body,
  Title,
  List,
  Badge,
  Spinner,
  CardItem,
  Icon,
  Content,
  Container,
  Tabs,
  Tab,
  H3,
  Accordion
} from "native-base";
import { Col, Row, Grid } from 'react-native-easy-grid';
import { FlatList, View, Dimensions, ScrollView } from "react-native";
import { VictoryChart, VictoryLine, VictoryTheme, VictoryVoronoiContainer, VictoryTooltip, VictoryAxis } from 'victory-native'
import { roundNumbers, getRandomColor, getCurrencyVal, chartWidth, getDateString, getTickCount, formatTick, getCardHeight } from './Constants'
import {styles} from './styles'

const CustomLabelComponent = (props) => {
  let newProps = {...props}
  newProps.style.push({fill: 'black'})
  delete newProps.text
  delete newProps.dy
  return <VictoryTooltip {...newProps} dy={20} text={[...props.text.map(textItem => getCurrencyVal(textItem)(2)), getDateString(props.datum.x)].join('\n')}/>
}

export default class SchemeDetailsComponent extends React.Component{
  constructor(props){
    super(props)
  }
  getSchemeDetails = (details) => (
    <Content padder>
      <Grid>
        <Col>

          <View style={styles.spacedView}>
            <Text style={styles.light}>Current Value</Text>
            <H3 style={styles.success}>{getCurrencyVal(roundNumbers(details.current_value))()}</H3>
          </View>

          <View style={styles.spacedView}>
            <Text style={styles.light}>Total Gain</Text>
            <H3 style={styles.success}>{getCurrencyVal(roundNumbers(details.total_gain))()}</H3>
          </View>
          <Row>
            <Col>
              <View style={styles.spacedView}>
                <Text style={styles.light}>Gain %</Text>
                <H3 style={styles.warning}>{roundNumbers(details.gain_percent)}</H3>
              </View>
            </Col>
            <Col>
              <View style={styles.spacedView}>
                <Text style={styles.light}>XIRR</Text>
                <H3 style={styles.warning}>{roundNumbers(details.xirr)}</H3>
              </View>
            </Col>
          </Row>

          <View style={styles.spacedView}>
            <Text style={styles.light}>Advisor Name</Text>
            <H3 style={styles.default}>{details.advisor}</H3>
          </View>

          <View style={styles.spacedView}>
            <Text style={styles.light}>Investor Name</Text>
            <H3 style={styles.default}>{details.investor}</H3>
          </View>
        </Col>
        <Col>
          <Right>
            <View style={styles.spacedView}>
              <Text style={styles.light}>Purchased Amount</Text>
              <H3 style={styles.default}>{getCurrencyVal(roundNumbers(details.purchase_amount))()}</H3>
            </View>

            <View style={styles.spacedView}>
              <Text style={styles.light}>Unrealised Gain</Text>
              <H3 style={styles.default}>{getCurrencyVal(roundNumbers(details.unrealised_gain))()}</H3>
            </View>

            <View style={styles.spacedView}>
              <Text style={styles.light}>Dividend</Text>
              <H3 style={styles.default}>{getCurrencyVal(roundNumbers(details.dividend))()}</H3>
            </View>

            <View style={styles.spacedView}>
              <Text style={styles.light}>Number of Units</Text>
              <H3 style={styles.default}>{getCurrencyVal(roundNumbers(details.units))()}</H3>
            </View>
          </Right>
        </Col>
      </Grid>
    </Content>
  )

  getCashFlow = (flows = []) => (
    <List containerStyle={{ padding: 0 }}>
      <ListItem>
        <Grid>
          <Col><Text style={styles.light}>Date</Text></Col>
          <Col><Text style={styles.light}>Advisor</Text></Col>
          <Col><Text style={styles.light}>Amount</Text></Col>
        </Grid>
      </ListItem>
      <ScrollView>
        {flows.map((item, index) => (
          <ListItem key={index}>
              <Grid>
                <Col><Text>{item.date}</Text></Col>
                <Col><Text style={styles.bold}>{item.advisor}</Text></Col>
                <Col>
                  <Text 
                    numberOfLines={1}
                    adjustsFontSizeToFit
                    style={styles[item.amount > 0 ? 'success' : 'danger']}>
                    {getCurrencyVal(Math.abs(item.amount))()}
                  </Text>
                </Col>
              </Grid>
          </ListItem>
        ))}
      </ScrollView>
    </List>
  )

  getComparison = (comparison) => {
    let colorMap = {}
    const getColor = (legendName) => {
      let randomColor = getRandomColor()
      colorMap[legendName] = randomColor
      return randomColor
    }
    const lines = Object.keys(comparison).map((comparisonItem, index) => {
      const color = getColor(comparisonItem)
      return (
        <VictoryLine
          key={index}
          style={{
            data: {stroke: color},
            parent: { border: "1px solid #ccc"},
            labels: {fill: color}
          }}
          data={comparison[comparisonItem].map(item => ({
            x: new Date(item.date),
            y: item.value
          }))}
        />
      )
    })

    const legend = (
      <Grid>
        <Col style={styles.legendContainer}>
          {Object.keys(colorMap).length ? Object.keys(colorMap).map(colorItem => (
            <Row key={colorMap[colorItem]}>
              <View style={[styles.legendDot, {backgroundColor: colorMap[colorItem]}]}/>
              <Text numberOfLines={2}>{` ${colorItem}`}</Text>
            </Row>
          )) : (
            <Row>
              <Text>{'No data to display'}</Text>
            </Row>
          )}
        </Col>
      </Grid>
    )

    return  (
      <React.Fragment>
        <VictoryChart 
          theme={VictoryTheme.material} 
          scale={{ x: "time" }}
          width={chartWidth()}
          minDomain={{ y: 0 }}
          tickFormat={{x: (t) => getDateforaxis(t)}}
          containerComponent={
            <VictoryVoronoiContainer
              voronoiDimension="x"
              labelComponent={<CustomLabelComponent />}
              labels={(d) => `${d.y}`}
            />
          }
        >
          <VictoryAxis 
            tickFormat={t=>formatTick(t)}
            tickCount={getTickCount()}
            crossAxis
          />
          <VictoryAxis 
            dependentAxis
            crossAxis
          />
          {lines}
        </VictoryChart>
        {legend}
      </React.Fragment>
    )
  }

  handleDimensionChange = (event) => {
    const {height} = event.nativeEvent.layout
    this.setState({height})
  }

  render(){
    const {schemeDetails, schemeName} = this.props
    const {details, flows, comparison} = Boolean(schemeDetails) ? schemeDetails : {}
    return (
      <Content padder>
        <Card style={{height: getCardHeight()}}>
          <CardItem header>
            <Title>{details && details.scheme_name || schemeName}</Title>
          </CardItem>
          {Boolean(schemeDetails) ? (
            <Tabs initialPage={2}>
              <Tab heading="Details" tabStyle={styles.tabStyle} activeTabStyle={styles.tabStyle}>
                {this.getSchemeDetails(details)}
              </Tab>
              <Tab heading="Cashflow" tabStyle={styles.tabStyle} activeTabStyle={styles.tabStyle}>
                {this.getCashFlow(flows)}
              </Tab>
              <Tab heading="Comparison" tabStyle={styles.tabStyle} activeTabStyle={styles.tabStyle}>
                {this.getComparison(comparison)}
              </Tab>
            </Tabs>
          ) : <Spinner color="black"/>}
        </Card>
      </Content>
    )
  }
}