import currency from 'currency.js'
import { Dimensions } from "react-native"
import {badgeStyleColors} from './styles'

const roundNumbers = num => Math.round(num * 10) / 10;

const xirrDeviation = (xirr, peer_xirr) => (xirr - peer_xirr) / Math.abs(peer_xirr)

const getBadgeType = (xirr, peer_xirr, dataMetrics) => {
  const [min, lowerMean, mean, higherMean, max] = [...dataMetrics]
  const dev = xirrDeviation(xirr, peer_xirr)
  if (dev === max) return badgeStyleColors.highest
  else if (dev === min) return badgeStyleColors.lowest
  else if (dev < lowerMean) return badgeStyleColors.lowerLowerMean
  else if (dev > higherMean) return badgeStyleColors.higherHigherMean
  else if (dev < mean) return badgeStyleColors.lowerMean
  else if (dev > mean) return badgeStyleColors.lowerHigherMean
}

const getRandomColor = () => {
  var letters = '0123456789ABCDEF';
  var color = '#';
  for (let i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}

const getCurrencyVal = (val = 0) => (precision = 0) => currency(val, {
  useVedic: true,
  formatWithSymbol: true,
  symbol: '₹',
  precision
}).format()

const BASE_URL = "https://www.altiorecapital.com/api/test/task_api_list/";
const SCHEME_URL = scheme_id => `https://www.altiorecapital.com/api/test/task_api_scheme/?scheme_id=${scheme_id}`;

const chartWidth = () => {
  let {width} = Dimensions.get('screen')
  return Math.floor(0.92 * width)
}

const getCardHeight = () => {
  let {height} = Dimensions.get('screen')
  return Math.floor(0.85 * height)
}

const getDateString = (d = '') => {
  const date = new Date(d)
  return `${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`
}

const monthMap = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']

const formatTick = t => {
  let date = new Date(t)
  return `${monthMap[date.getMonth()]}'${date.getFullYear()}`
}

const getTickCount = () => {
  const {width, height} = Dimensions.get('screen')
  return height > width ? 4 : 5
}

const performDataAnalysis = (dataset = []) => {
  const deviationArray = dataset.map(investmentItem => xirrDeviation(investmentItem.xirr, investmentItem.peer_xirr))
  const max = Math.max(...deviationArray)
  const min = Math.min(...deviationArray)
  const mean = (max-min)/2
  return [min, (mean-min)/2 ,mean, (max-mean)/2 ,max]
}

export {
  roundNumbers,
  getBadgeType,
  BASE_URL,
  SCHEME_URL,
  getRandomColor,
  getCurrencyVal,
  chartWidth,
  getCardHeight,
  getDateString,
  getTickCount,
  formatTick,
  performDataAnalysis
}