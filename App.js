import React from "react";
import { View, ScrollView, RefreshControl } from "react-native";
import {
  Button,
  Text,
  ListItem,
  Header,
  Card,
  Left,
  Right,
  Body,
  Title,
  List,
  Badge,
  Spinner,
  CardItem,
  Icon,
  Content,
  Container
} from "native-base";
import SchemeDetailsComponent from './SchemeDetailsComponent'
import {performDataAnalysis, BASE_URL, SCHEME_URL, getCurrencyVal, getBadgeType, roundNumbers} from './Constants'
import {styles} from './styles'

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      schemeDetails: null,
      selectedSchemeName: null,
      dataMetrics: null
    };
  }

  makeAPICallsAndSave = (url, stateKey) => {
    fetch(url)
      .then(response => response.json())
      .then(data => {
        let state = {}
        if(data && data.dataset) {
          state = {
            [stateKey]: data.dataset, 
            dataMetrics: performDataAnalysis(data.dataset)
          }
        } else {
          state = {
            [stateKey]: data
          }
        }
        this.setState({...state})
      });
  };

  componentWillMount() {
    this.makeAPICallsAndSave(BASE_URL, "data");
  }

  prepareSchemeScreen = (scheme_id, scheme_name) => {
    this.setState({selectedSchemeName: scheme_name},() => {
      this.makeAPICallsAndSave(SCHEME_URL(scheme_id), "schemeDetails");
    })
  }

  onRefresh = () => {
    this.setState({data: []}, () => this.makeAPICallsAndSave(BASE_URL, "data"))
  }

  getList = data => (
    <ScrollView
      refreshControl={
        <RefreshControl
          refreshing={!Boolean(this.state.data && this.state.data.length)}
          onRefresh={this.onRefresh.bind(this)}
        />
      }
    >
      <List containerStyle={{ padding: 0 }}>
        <ListItem selected noIndent>
          <Left>
            <Text style={styles.light}>Scheme Name</Text>
          </Left>
          <Text style={styles.light}>Value</Text>
          <Right>
            <Text style={styles.light}>XIRR %</Text>
          </Right>
        </ListItem>
        {data.map(dataItem => (
          <ListItem
            key={dataItem.scheme_id}
            onPress={() => this.prepareSchemeScreen(dataItem.scheme_id, dataItem.scheme_name)}
          >
            <Left>
              <Text>{dataItem.scheme_name}</Text>
            </Left>
            <Text>{getCurrencyVal(dataItem.current_value)()}</Text>
            <Right>
              <Badge style={getBadgeType(dataItem.xirr, dataItem.peer_xirr, this.state.dataMetrics)}>
                <Text>{roundNumbers(dataItem.xirr)}</Text>
              </Badge>
            </Right>
          </ListItem>
        ))}
      </List>
    </ScrollView>
  );

  getExplorerView = () => this.state.data.length ? this.getList(this.state.data) : <Spinner color="black" />;

  getSchemeView = (schemeDetails) => <SchemeDetailsComponent schemeDetails={schemeDetails} schemeName={this.state.selectedSchemeName}/>

  getHeader = (val) => val ? (
    <Header style={styles.headerStyle}>
      <Left>
        <Button transparent onPress={() => this.setState({schemeDetails: null, selectedSchemeName: null})}>
          <Text style={styles.headerTextColor}>Back</Text>
        </Button>
      </Left>
      <Body>
        <Title style={styles.headerTextColor}>Explorer</Title>
      </Body>
      <Right />
    </Header>
  ) : (
    <Header style={styles.headerStyle}>
      <Left />
      <Body>
        <Title style={styles.headerTextColor}>Explorer</Title>
      </Body>
      <Right />
    </Header>
  )

  render() {
    return (
      <Container>
        {this.getHeader(Boolean(this.state.selectedSchemeName))}
        {Boolean(this.state.selectedSchemeName) ? this.getSchemeView(this.state.schemeDetails) : this.getExplorerView()}
      </Container>
    );
  }
}
