import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  headerStyle: {
    backgroundColor: '#0d61cf'
  },
  headerTextColor: {
    color: 'white'
  },
  legendDot: {
    width: 15,
    height: 15,
    borderRadius: 15/2
  },
  legendContainer: {
    borderWidth: 0.5, 
    padding: 5
  },
  tabStyle: {
    backgroundColor: 'white'
  },
  success: {
    color: 'seagreen',
    fontWeight: 'bold'
  },
  danger: {
    color: 'darkred',
    fontWeight: 'bold'
  },
  default: {
    color: "#0d61cf",
    fontWeight: 'bold'
  },
  warning: {
    color: "darkorange",
    fontWeight: 'bold'
  },
  bold: {
    fontWeight: 'bold'
  },
  spacedView: {
    margin: 5
  },
  light: {
    color: 'darkgray'
  }
});

const badgeStyleColors = StyleSheet.create({
  lowest: {
    backgroundColor: 'crimson'
  },
  lowerLowerMean: {
    backgroundColor: 'gold'
  },
  lowerMean: {
    backgroundColor: 'palegoldenrod'
  },
  lowerHigherMean: {
    backgroundColor: 'palegreen'
  },
  higherHigherMean: {
    backgroundColor: 'mediumseagreen'
  },
  highest: {
    backgroundColor: 'seagreen'
  }
})

export {
  styles,
  badgeStyleColors
}