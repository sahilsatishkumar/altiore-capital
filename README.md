# SPEC

**Screen 1** :  
A list view which displays schemes, their value and their performance or "XIRR".  
API : `https://www.altiorecapital.com/api/test/task_api_list/`  
The data from this api, is an array of objects, each of which corresponds to each row in the list.   
Each object has 5 elements:
1. scheme_id (used to pass to screen 2)
2. scheme_name (to be displayed)
3. value (to be displayed)
4. xirr (to be displayed)
5. peer_xirr (used to calculate colour of XIRR)

To calculate the background colour of the XIRR, you need to find the deviation between the peer_xirr and xirr.

This is calculated by `(xirr-peer_xirr)/abs(peer_xirr)`. Colour it based on this deviation, where the most positive is shown with dark green, the most negative is shown with dark red and all other values in between will range between red and green. You can choose the variation in colour for the in between values. 

![Screen1](./assets/screen1.png)

On tapping any row in screen 1, push another screen with three tabs (screen 2,3,4)

**Screen 2,3,4**: is rendered after accessing  
 `https://www.altiorecapital.com/api/test/task_api_scheme/?scheme_id="[value of Scheme Id from screen 1]"`

**Screen 2 :**  
Displays some additional data related to the row tapped in screen 1.  
The object named "details" from the second api is used to get the following data :
1. current_value
2. units
3. purchase_amount
4. dividend
5. unrealised_gain
6. scheme_name (displayed on top, eg 8.20% NHAI LTD 10YRS)
7. xirr
8. investor  (Investor Name)
9. total_gain 
10. advisor (Advisor Name)
11. gain_percent (Gain %)

![Screen 2](./assets/screen2.png)

**Screen 3 :**  
Displays a list. The object "flows" from the second api is used to retrieve an array of objects where each object displays a row in following list
1. date
2. amount
3. advisor

![Screen 3](./assets/screen3.png)


**Screen 4 :**  
Displays a graph. You can use any plugin you are familiar with to implement it.  
The object "comparison" retrieves an object of three objects. Each object represents a particular line(eg 8.20% NHAI LTD 10YRS). The object returns an array with the variables:
1. date
2. value  

Each date value pair corresponds to a point to be plotted in each line.  
Incase no data is available, the value of "comparison" will be null.  
You would need to display a tooltip when tapping a point in the graph. The tooltip should show the corresponding scheme name and value for all the schemes in the graph (max 3 schemes at a time).  

![Screen 4](./assets/screen4.png)


**Optional Bonus task for extra points :**  
1)In screen 1, all the data is sent at one time. Your task would be to display 10 rows at a time to improve performance. Lazy loading a set of 10 rows should be done when the user scrolls to the bottom of the screen.

You can test this app yourself on any iOS device to scan this code through the expo app `https://expo.io/@sahrckr/altiore-capital`